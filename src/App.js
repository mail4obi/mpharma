import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Home from './screens/Home';
import NewProduct from './screens/NewProduct';
import EditProduct from './screens/EditProduct';

function App() {
  return (
    <Router>
      <div>
        <div className="ui menu">
          <Link className="item" to="/">Home</Link>
          <Link className="item" to="/newproduct/">New Product</Link>
        </div>

        <Route path="/" exact component={Home} />
        <Route path="/newproduct" exact component={NewProduct} />
        <Route path="/editproduct/:id" exact component={EditProduct} />
      </div>
    </Router>
  );
}

export default App;
