import { uniqBy, last, sortBy } from 'lodash';

import {
  FETCH_PRODUCTS,
  FETCHING_PRODUCTS,
  ADD_PRODUCT,
  UPDATE_PRODUCT,
} from '../actions/types';

const initialState = {
  fetchingProducts: false,
  products: [],
};

export default (state = initialState, action) => {
  let id;
  let lastProduct;
  switch (action.type) {
    case FETCHING_PRODUCTS:
      return { ...state, fetchingProducts: true };
    case FETCH_PRODUCTS:
      return {
        ...state,
        fetchingProducts: false,
        products: action.error ? state.products : uniqBy([...action.data, ...state.products], 'id'),
      };
    case ADD_PRODUCT:
      lastProduct = last(sortBy(state.products, 'id'));
      id = lastProduct ? lastProduct.id + 1 : 1;
      return {
        ...state,
        products: [...state.products, {
          name: action.data.name,
          id,
          prices: [{ id: 1, price: action.data.price, date: new Date().toISOString() }],
        }],
      };
    case UPDATE_PRODUCT:
      return {
        ...state,
      };
    default:
      return state;
  }
};
