import { connect } from 'react-redux';

import {
  fetchProducts,
} from '../../actions/product';

import Home from './Home';

const mapStateToProps = (state) => state.main;

const mapDispatchToProps = {
  fetchProducts,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
