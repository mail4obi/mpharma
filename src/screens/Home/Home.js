import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Dimmer,
  Loader,
  Segment,
  Card,
} from 'semantic-ui-react';

import Product from '../../components/product';

class Home extends Component {
  componentDidMount() {
    const { fetchProducts } = this.props;
    fetchProducts();
  }

  render() {
    const { fetchingProducts, products } = this.props;
    return (
      <Segment basic>
        <Dimmer inverted active={fetchingProducts}>
          <Loader inverted>Loading</Loader>
        </Dimmer>

        <Card.Group centered>
          {
            products.map((product) => (
              <Product key={product.id} data={product} />
            ))
          }
        </Card.Group>
      </Segment>
    );
  }
}

Home.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
  fetchingProducts: PropTypes.bool.isRequired,
  fetchProducts: PropTypes.func.isRequired,
};

export default Home;
