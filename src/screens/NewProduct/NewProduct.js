/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-alert */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Segment,
  Form,
  Button,
} from 'semantic-ui-react';


class NewProduct extends Component {
  constructor(props) {
    super(props);
    this.addProduct = this.addProduct.bind(this);
    this.state = {
      name: '',
      price: '',
    };
  }

  setValue(name, event) {
    this.setState({ [name]: event.target.value });
  }

  addProduct() {
    const { addProduct } = this.props;
    const { name, price } = this.state;
    addProduct({
      name, price,
    });
    document.forms[0].reset();
    this.setState({ name: '', price: '' });
    alert('product added');
  }

  render() {
    return (
      <Segment basic>
        <Form id="form">
          <Form.Field>
            <label htmlFor="name">Product Name</label>
            <input id="name" placeholder="Product Name" onChange={this.setValue.bind(this, 'name')} />
          </Form.Field>
          <Form.Field>
            <label htmlFor="price">Product Price</label>
            <input id="price" placeholder="Product Price" type="number" onChange={this.setValue.bind(this, 'price')} />
          </Form.Field>
          <Button type="submit" onClick={this.addProduct}>Add</Button>
        </Form>
      </Segment>
    );
  }
}

NewProduct.propTypes = {
  addProduct: PropTypes.func.isRequired,
};

export default NewProduct;
