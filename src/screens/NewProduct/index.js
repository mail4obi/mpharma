import { connect } from 'react-redux';

import {
  addProduct,
} from '../../actions/product';

import NewProduct from './NewProduct';

const mapStateToProps = null;

const mapDispatchToProps = {
  addProduct,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);
