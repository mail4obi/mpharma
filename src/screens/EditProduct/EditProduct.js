/* eslint-disable no-alert */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Segment,
  Form,
  Button,
} from 'semantic-ui-react';

class EditProduct extends Component {
  constructor(props) {
    super(props);
    this.updateProduct = this.updateProduct.bind(this);
    this.state = {
      name: '',
      price: '',
    };
  }

  setValue(name, event) {
    this.setState({ [name]: event.target.value });
  }

  updateProduct() {
    const { updateProduct, product: { id } } = this.props;
    const { name, price } = this.state;
    updateProduct(id, {
      name, price,
    });
    document.forms[0].reset();
    this.setState({ name: '', price: '' });
    alert('product updated');
  }

  render() {
    const { product } = this.props;
    return (
      <Segment basic>
        <Form id="form">
          <Form.Field>
            <label htmlFor="name">Product Name</label>
            <input id="name" defaultValue={product.name} placeholder="Product Name" onChange={this.setValue.bind(this, 'name')} />
          </Form.Field>
          <Form.Field>
            <label htmlFor="price">Product Price</label>
            <input id="price" placeholder="Product Price" type="number" onChange={this.setValue.bind(this, 'price')} />
          </Form.Field>
          <Button type="submit" onClick={this.updateProduct}>Update</Button>
        </Form>
      </Segment>
    );
  }
}

EditProduct.propTypes = {
  updateProduct: PropTypes.func.isRequired,
  product: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    prices: PropTypes.array,
  }).isRequired,
};

export default EditProduct;
