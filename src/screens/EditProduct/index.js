import { connect } from 'react-redux';
import { find } from 'lodash';

import {
  updateProduct,
} from '../../actions/product';

import EditProduct from './EditProduct';

const mapStateToProps = (state, props) => {
  const { match: { params: { id } } } = props;
  const product = find(state.main.products, (p) => Number(p.id) === Number(id));
  return {
    product: product || {},
  };
};

const mapDispatchToProps = {
  updateProduct,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProduct);
