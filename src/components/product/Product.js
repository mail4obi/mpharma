import React from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  Image,
  Button,
} from 'semantic-ui-react';
import { last, sortBy } from 'lodash';
import { Link } from 'react-router-dom';

import './style.css';

const Product = ({ data: { id, name, prices }, onDelete }) => {
  const { price } = last(sortBy(prices, (p) => new Date(p.date).getTime()));
  return (
    <Card>
      <Image className="product-image" src="https://png.icons8.com/gift/ultraviolet/200/3498db" />
      <Card.Content>
        <Card.Header content={name} />
        <Card.Meta content={`Price: ${price}`} />
        <Link className="ui button" to={`/editproduct/${id}`}>Edit</Link>
        <Button onClick={onDelete}>Delete</Button>
      </Card.Content>
    </Card>
  );
};

Product.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number, name: PropTypes.string, prices: PropTypes.array,
  }).isRequired,
  onDelete: PropTypes.func,
};

Product.defaultProps = {
  onDelete: () => { },
};

export default Product;
