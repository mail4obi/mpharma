import axios from 'axios';

import {
  FETCH_PRODUCTS,
  FETCHING_PRODUCTS,
  ADD_PRODUCT,
  UPDATE_PRODUCT,
} from './types';

export const fetchProducts = () => async (dispatch) => {
  try {
    dispatch({
      type: FETCHING_PRODUCTS,
    });

    const { data: { products } } = await axios('http://www.mocky.io/v2/5c3e15e63500006e003e9795');

    dispatch({
      type: FETCH_PRODUCTS,
      data: products,
    });
  } catch (error) {
    dispatch({
      type: FETCH_PRODUCTS,
      error: error.message,
    });
  }
};

/**
 *
 * @param {{ name, price }} data
 */
export const addProduct = (data) => ({
  type: ADD_PRODUCT,
  data,
});

/**
 * @param {Number} id
 * @param {{ name, price }} data
 */
export const updateProduct = (id, data) => ({
  type: UPDATE_PRODUCT,
  data: { id, update: data },
});
